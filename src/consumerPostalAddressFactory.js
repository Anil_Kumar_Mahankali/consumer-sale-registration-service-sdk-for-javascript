import ConsumerPostalAddress from './consumerPostalAddress';

export default class ConsumerPostalAddressFactory { 

    static construct(data):ConsumerPostalAddress {

        const addressLine1 = data.addressLine1;

        const addressLine2 = data.addressLine2;

        const city = data.city;

        const regionIso31662Code = data.regionIso31662Code;

        const postalCode = data.postalCode;

        const countryIso31661Alpha2Code = data.countryIso31661Alpha2Code;

        return new ConsumerPostalAddress(
            addressLine1,
            addressLine2,
            city,
            regionIso31662Code,
            postalCode,
            countryIso31661Alpha2Code
        );


    }
}