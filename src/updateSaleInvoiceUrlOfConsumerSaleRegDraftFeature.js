import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ConsumerSaleRegistrationServiceSdkConfig from './consumerSaleRegistrationServiceSdkConfig';
import UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq from './updateSaleInvoiceUrlOfConsumerSaleRegDraftReq';

@inject(ConsumerSaleRegistrationServiceSdkConfig, HttpClient)
class UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature {

    _config:ConsumerSaleRegistrationServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ConsumerSaleRegistrationServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     *
     * @param request
     * @param accessToken
     */
    execute(request:UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq,
            accessToken:string) {

        return this._httpClient
            .createRequest(`consumer-sale-registration/${request.id}/updateInvoiceUrl`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request.invoiceUrl)
            .send()

    }
}

export default UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature;
