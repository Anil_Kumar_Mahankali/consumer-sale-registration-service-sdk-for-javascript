import DiContainer from './diContainer';
import ConsumerSaleRegistrationServiceSdkConfig from './consumerSaleRegistrationServiceSdkConfig';
import AddConsumerSaleRegDraftReq from './addConsumerSaleRegDraftReq';
import AddConsumerSaleRegDraftFeature from './addConsumerSaleRegDraftFeature';
import ConsumerSaleRegSynopsisView from './consumerSaleRegSynopsisView';
import GetConsumerSaleRegDraftWithIdFeature from './getConsumerSaleRegDraftWithIdFeature';
import GetConsumerSaleRegDraftWithAccountIdFeature from './getConsumerSaleRegDraftWithAccountIdFeature';
import UpdateConsumerSaleRegDraftReq from './updateConsumerSaleRegDraftReq';
import UpdateConsumerSaleRegDraftFeature from './updateConsumerSaleRegDraftFeature';
import DeleteConsumerSaleRegDraftWithIdFeature from './deleteConsumerSaleRegDraftWithIdFeature';
import UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq from './updateSaleInvoiceUrlOfConsumerSaleRegDraftReq';
import ProductSaleRegistrationRuleEngineWebDto from './productSaleRegistrationRuleEngineWebDto';
import SubmitConsumerSaleRegDraftDto from './submitConsumerSaleRegDraftDto';
import UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature from './updateSaleInvoiceUrlOfConsumerSaleRegDraftFeature';
import RemoveSaleLineItemFromConsumerSaleRegDraftFeature from './removeSaleLineItemFromConsumerSaleRegDraftFeature';
import SubmitConsumerSaleRegDraftFeature from './submitConsumerSaleRegDraftFeature';
import IsEligibleForSpiffFeature from './isEligibleForSpiffFeature';
import GetConsumerSaleDraftExcludeSerialNumberWithDraftId from './getConsumerSaleDraftExcludeSerialNumberWithDraftId';

/**
 * @class {ConsumerSaleRegistrationServiceSdk}
 */
export default class ConsumerSaleRegistrationServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {ConsumerSaleRegistrationServiceSdkConfig} config
     */
    constructor(config:ConsumerSaleRegistrationServiceSdkConfig) {

        this._diContainer = new DiContainer(config);
    }

    /**
     *
     * @param request
     * @param accessToken
     * @returns {Promise.<number>}
     */
    addConsumerSaleRegDraft(request:AddConsumerSaleRegDraftReq,
                            accessToken:string):Promise<number> {

        return this
            ._diContainer
            .get(AddConsumerSaleRegDraftFeature)
            .execute(
                request,
                accessToken
            );

    }

    /**
     * @param consumerSaleRegDraftId
     * @param accessToken
     * @returns {Promise.<ConsumerSaleRegSynopsisView>}
     */
    getConsumerSaleRegDraftWithId(consumerSaleRegDraftId:number,
                                  accessToken:string):Promise<ConsumerSaleRegSynopsisView> {

        return this
            ._diContainer
            .get(GetConsumerSaleRegDraftWithIdFeature)
            .execute(
                consumerSaleRegDraftId,
                accessToken
            );

    }


    /**
     * @param partnerAccountId
     * @param accessToken
     * @returns {Promise.<ConsumerSaleRegSynopsisView[]>}
     */
    getConsumerSaleRegDraftWithAccountId(partnerAccountId:string,
                                         accessToken:string):Promise<ConsumerSaleRegSynopsisView[]> {

        return this
            ._diContainer
            .get(GetConsumerSaleRegDraftWithAccountIdFeature)
            .execute(
                partnerAccountId,
                accessToken
            );

    }

    /**
     *
     * @param request
     * @param consumerSaleRegDraftId
     * @param accessToken
     * @returns {ConsumerSaleRegSynopsisView}
     */
    updateConsumerSaleRegDraft(request:UpdateConsumerSaleRegDraftReq,
                               consumerSaleRegDraftId:number,
                               accessToken:string):Promise<ConsumerSaleRegSynopsisView> {
        return this
            ._diContainer
            .get(UpdateConsumerSaleRegDraftFeature)
            .execute(
                request,
                consumerSaleRegDraftId,
                accessToken
            );

    }

    /**
     *
     * @param consumerSaleRegDraftId
     * @param accessToken
     */
    deleteConsumerSaleRegDraftWithId(consumerSaleRegDraftId:number, accessToken:string) {

        return this
            ._diContainer
            .get(DeleteConsumerSaleRegDraftWithIdFeature)
            .execute(
                consumerSaleRegDraftId,
                accessToken
            );

    }

    /**
     * @param request
     * @param accessToken
     * @returns {string}
     */
    updateSaleInvoiceUrlOfConsumerSaleRegDraft(request:UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq,
                                               accessToken:string) {

        return this
            ._diContainer
            .get(UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature)
            .execute(
                request,
                accessToken
            );

    }

    /**
     * @param consumerSaleRegDraftSaleLineItemId
     * @param accessToken
     */
    removeSaleLineItemFromConsumerSaleRegDraft(consumerSaleRegDraftSaleLineItemId:number,
                                               accessToken:string){

        return this
            ._diContainer
            .get(RemoveSaleLineItemFromConsumerSaleRegDraftFeature)
            .execute(
                consumerSaleRegDraftSaleLineItemId,
                accessToken
            );

    }

    /**
     * @param {SubmitConsumerSaleRegDraftDto} request
     * @param {string} accessToken
     * @returns {Promise.<number>|Promise.<ConsumerSaleRegSynopsisView>}
     */
    submitConsumerSaleRegDraft(request:SubmitConsumerSaleRegDraftDto,
                               accessToken:string):Promise<ConsumerSaleRegSynopsisView> {

        return this
            ._diContainer
            .get(SubmitConsumerSaleRegDraftFeature)
            .execute(
                request,
                accessToken
            );

    }

    /**
     * @param {ProductSaleRegistrationRuleEngineWebDto} request
     * @param {string} accessToken
     * @returns {boolean}
     */
    isEligibleForSpiff(request:ProductSaleRegistrationRuleEngineWebDto,
                       accessToken:string):Promise<boolean>{

        return this
            ._diContainer
            .get(IsEligibleForSpiffFeature)
            .execute(
                request,
                accessToken
            )

    }

    /**
     * @param consumerSaleRegDraftId
     * @param accessToken
     * @returns {Promise.<ConsumerSaleRegSynopsisView>}
     */
    getConsumerSaleDraftExcludeSerialNumberWithDraftId(consumerSaleRegDraftId:number,
                                  accessToken:string):Promise<ConsumerSaleRegSynopsisView> {

        return this
            ._diContainer
            .get(GetConsumerSaleDraftExcludeSerialNumberWithDraftId)
            .execute(
                consumerSaleRegDraftId,
                accessToken
            );

    }
}
