/**
 * @class {ConsumerPostalAddress}
 */

export default class ConsumerPostalAddress { 

    _addressLine1:string;

    _addressLine2:string;

    _city:string;

    _regionIso31662Code:string;

    _postalCode:string;

    _countryIso31661Alpha2Code:string;

    /**
     * @param {string} addressLine1
     * @param {string} addressLine2
     * @param {string} city
     * @param {string} regionIso31662Code ISO 3166-2 codegi
     * @param {string} postalCode
     * @param {string} countryIso31661Alpha2Code ISO 3166-1-Alpha2 code
     */
    constructor(addressLine1:string,
                addressLine2:string,
                city:string,
                regionIso31662Code:string,
                postalCode:string,
                countryIso31661Alpha2Code:string) {

        if (!addressLine1) {
            throw new TypeError('addressLine1 required');
        }
        this._addressLine1 = addressLine1;

        this._addressLine2 = addressLine2;

        if (!city) {
            throw new TypeError('city required');
        }
        this._city = city;

        if (!regionIso31662Code) {
            throw new TypeError('regionIso31662Code required');
        }
        this._regionIso31662Code = regionIso31662Code;

        if (!postalCode) {
            throw new TypeError('postalCode required');
        }
        this._postalCode = postalCode;

        if (!countryIso31661Alpha2Code) {
            throw new TypeError('countryIso31661Alpha2Code required');
        }
        this._countryIso31661Alpha2Code = countryIso31661Alpha2Code;

    }

    /**
     * @returns {string}
     */
    get addressLine1():string {
        return this._addressLine1;
    }

    /**
     * @returns {string}
     */
    get addressLine2():string {
        return this._addressLine2;
    }

    /**
     * @returns {string}
     */
    get city():string {
        return this._city;
    }

    /**
     * @returns {string}
     */
    get regionIso31662Code():string {
        return this._regionIso31662Code;
    }

    /**
     * @returns {string}
     */
    get postalCode():string {
        return this._postalCode;
    }

    /**
     * @returns {string}
     */
    get countryIso31661Alpha2Code():string {
        return this._countryIso31661Alpha2Code;
    }

    toJSON() {
        return {
            addressLine1: this._addressLine1,
            addressLine2: this._addressLine2,
            city: this._city,
            regionIso31662Code: this._regionIso31662Code,
            postalCode: this._postalCode,
            countryIso31661Alpha2Code: this._countryIso31661Alpha2Code
        }
    }

}

