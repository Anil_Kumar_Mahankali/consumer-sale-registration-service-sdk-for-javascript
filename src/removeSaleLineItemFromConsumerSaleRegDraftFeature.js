import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ConsumerSaleRegistrationServiceSdkConfig from './consumerSaleRegistrationServiceSdkConfig';

@inject(ConsumerSaleRegistrationServiceSdkConfig, HttpClient)
class RemoveSaleLineItemFromConsumerSaleRegDraftFeature {

    _config:ConsumerSaleRegistrationServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ConsumerSaleRegistrationServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * @param consumerSaleRegDraftSaleLineItemId
     * @param accessToken
     */
    execute(consumerSaleRegDraftSaleLineItemId:number,
            accessToken:string) {

        return this._httpClient
            .createRequest(`consumer-sale-registration/removeSaleLineItem/${consumerSaleRegDraftSaleLineItemId}`)
            .asDelete()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()

    }
}

export default RemoveSaleLineItemFromConsumerSaleRegDraftFeature;
