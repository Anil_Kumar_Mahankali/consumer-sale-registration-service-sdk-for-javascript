import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import AddConsumerSaleRegDraftReq from './addConsumerSaleRegDraftReq';
import ConsumerSaleRegistrationServiceSdkConfig from './consumerSaleRegistrationServiceSdkConfig';

@inject(ConsumerSaleRegistrationServiceSdkConfig, HttpClient)
class AddConsumerSaleRegDraftFeature {

    _config:ConsumerSaleRegistrationServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ConsumerSaleRegistrationServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     *
     * @param request
     * @param accessToken
     * @returns {Promise.<number>}
     */
    execute(request:AddConsumerSaleRegDraftReq,
            accessToken:string):Promise<number> {

        return this._httpClient
            .createRequest('consumer-sale-registration')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);
    }
}

export default AddConsumerSaleRegDraftFeature;