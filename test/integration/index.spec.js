import ConsumerSaleRegistrationServiceSdk,{
    AddConsumerSaleRegDraftReq,
    ConsumerSaleRegSynopsisView,
    UpdateConsumerSaleRegDraftReq,
    UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq,
    SubmitConsumerSaleRegDraftDto
} from '../../src/index';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be ConsumerSaleRegistrationServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new ConsumerSaleRegistrationServiceSdk(config.consumerSaleRegistrationServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(ConsumerSaleRegistrationServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('addConsumerSaleRegDraft method', () => {
            it('should return id', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new ConsumerSaleRegistrationServiceSdk(config.consumerSaleRegistrationServiceSdkConfig);

                let addConsumerSaleRegDraftReq =
                    new AddConsumerSaleRegDraftReq(
                        dummy.firstName,
                        dummy.lastName,
                        dummy.address,
                        dummy.phoneNumber,
                        dummy.personEmail,
                        dummy.sellDate,
                        dummy.invoiceNumber,
                        dummy.partnerAccountId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                /*
                 act
                 */
                const actPromise =
                    objectUnderTest
                        .addConsumerSaleRegDraft(
                            addConsumerSaleRegDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */
                actPromise
                    .then(id => {
                        expect(id).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
        });

        describe('getConsumerSaleRegDraftWithId method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new ConsumerSaleRegistrationServiceSdk(config.consumerSaleRegistrationServiceSdkConfig);

                let addConsumerSaleRegDraftReq =
                    new AddConsumerSaleRegDraftReq(
                        dummy.firstName,
                        dummy.lastName,
                        dummy.address,
                        dummy.phoneNumber,
                        dummy.personEmail,
                        dummy.sellDate,
                        dummy.invoiceNumber,
                        dummy.partnerAccountId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                // seed a new account so we can test the retrieval of it
                const actPromise =
                    objectUnderTest
                        .addConsumerSaleRegDraft(
                            addConsumerSaleRegDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 act
                 */
                const getConsumerSaleRegDraftWithIdPromise =
                    actPromise
                        .then(id =>
                             objectUnderTest
                                .getConsumerSaleRegDraftWithId(
                                    id,
                                    factory
                                        .constructValidPartnerRepOAuth2AccessToken()
                                )
                        );


                /*
                 assert
                 */
                getConsumerSaleRegDraftWithIdPromise
                    .then((ConsumerSaleRegSynopsisView) => {
                        expect(ConsumerSaleRegSynopsisView).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });

        describe('getConsumerSaleRegDraftWithAccountId method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new ConsumerSaleRegistrationServiceSdk(config.consumerSaleRegistrationServiceSdkConfig);


                /*
                 act
                 */
                const actPromise =
                    objectUnderTest
                        .getConsumerSaleRegDraftWithAccountId(
                            config.accountIdOfExistingAccountWithAnSapAccountNumber,
                            factory
                                .constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */
                actPromise
                    .then((ConsumerSaleRegSynopsisView) => {
                        expect(ConsumerSaleRegSynopsisView.length >= 0).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 50000)
        });

        describe('updateConsumerSaleRegDraft method', () => {
            it('should return ConsumerSaleRegSynopsisView', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new ConsumerSaleRegistrationServiceSdk(config.consumerSaleRegistrationServiceSdkConfig);

                const addConsumerSaleRegDraftReq =
                    new AddConsumerSaleRegDraftReq(
                        dummy.firstName,
                        dummy.lastName,
                        dummy.address,
                        dummy.phoneNumber,
                        dummy.personEmail,
                        dummy.sellDate,
                        dummy.invoiceNumber,
                        dummy.partnerAccountId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                // seed a new account so we can test the retrieval of it
                const actPromise =
                    objectUnderTest
                        .addConsumerSaleRegDraft(
                            addConsumerSaleRegDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 act
                 */
                const updateConsumerSaleRegDraftPromise =
                    actPromise.then(id =>{
                        let updateConsumerSaleRegDraftReq =
                            new UpdateConsumerSaleRegDraftReq(
                                id,
                                dummy.firstName,
                                dummy.lastName,
                                dummy.address,
                                dummy.phoneNumber,
                                dummy.personEmail,
                                dummy.sellDate,
                                dummy.invoiceNumber,
                                dummy.partnerAccountId,
                                dummy.partnerRepUserId,
                                dummy.invoiceUrl,
                                dummy.isSubmitted,
                                factory.constructSimpleLineItems(),
                                factory.constructCompositeLineItems(),
                                dummy.createDate
                            );
                            return objectUnderTest
                                .updateConsumerSaleRegDraft(
                                    updateConsumerSaleRegDraftReq,
                                    id,
                                    factory
                                        .constructValidPartnerRepOAuth2AccessToken()
                                );
                        }
                    );


                /*
                 assert
                 */
                updateConsumerSaleRegDraftPromise
                    .then((ConsumerSaleRegSynopsisView) => {
                        expect(ConsumerSaleRegSynopsisView).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });



        describe('updateSaleInvoiceUrlOfConsumerSaleRegDraft method', () => {
            it('should return success message', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new ConsumerSaleRegistrationServiceSdk(config.consumerSaleRegistrationServiceSdkConfig);

                let addConsumerSaleRegDraftReq =
                    new AddConsumerSaleRegDraftReq(
                        dummy.firstName,
                        dummy.lastName,
                        dummy.address,
                        dummy.phoneNumber,
                        dummy.personEmail,
                        dummy.sellDate,
                        dummy.invoiceNumber,
                        dummy.partnerAccountId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                // seed a new account so we can test the retrieval of it
                const actPromise =
                    objectUnderTest
                        .addConsumerSaleRegDraft(
                            addConsumerSaleRegDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 act
                 */
                const updateSaleInvoiceUrlOfConsumerSaleRegDraftPromise =
                    actPromise
                        .then(id =>
                             objectUnderTest
                                .updateSaleInvoiceUrlOfConsumerSaleRegDraft(
                                    new UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq(
                                        id,
                                        "http://www.new-dummy-url.com"
                                    ),
                                    factory
                                        .constructValidPartnerRepOAuth2AccessToken()
                                )
                    );

                /*
                 assert
                 */
                updateSaleInvoiceUrlOfConsumerSaleRegDraftPromise
                    .then(() => {
                        //expect(string).toEqual('Invoice Url updated successfully');
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });

        describe('submitConsumerSaleRegDraft method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new ConsumerSaleRegistrationServiceSdk(config.consumerSaleRegistrationServiceSdkConfig);

                let addConsumerSaleRegDraftReq =
                    new AddConsumerSaleRegDraftReq(
                        dummy.firstName,
                        dummy.lastName,
                        dummy.address,
                        dummy.phoneNumber,
                        dummy.personEmail,
                        dummy.sellDate,
                        dummy.invoiceNumber,
                        dummy.partnerAccountId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );


                // seed a new account so we can test the retrieval of it
                const actPromise =
                    objectUnderTest
                        .addConsumerSaleRegDraft(
                            addConsumerSaleRegDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 act
                 */
                const submitConsumerSaleRegDraftPromise =
                    actPromise.then(id =>
                        objectUnderTest
                            .submitConsumerSaleRegDraft(
                                new SubmitConsumerSaleRegDraftDto(
                                    id,
                                    dummy.submittedByName,
                                    dummy.partnerRepUserId,
                                    dummy.firstName,
                                    dummy.lastName,
                                    dummy.personEmail
                                ),
                                factory.constructValidAppAccessToken()
                            )
                    );

                /*
                 assert
                 */
                submitConsumerSaleRegDraftPromise
                    .then(ConsumerSaleRegSynopsisView => {
                        expect(ConsumerSaleRegSynopsisView).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });

        describe('deleteConsumerSaleRegDraftWithId method', () => {
            it('should delete Consumer Draft', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new ConsumerSaleRegistrationServiceSdk(config.consumerSaleRegistrationServiceSdkConfig);

                const addConsumerSaleRegDraftReq =
                    new AddConsumerSaleRegDraftReq(
                        dummy.firstName,
                        dummy.lastName,
                        dummy.address,
                        dummy.phoneNumber,
                        dummy.personEmail,
                        dummy.sellDate,
                        dummy.invoiceNumber,
                        dummy.partnerAccountId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                // seed a new account so we can test the retrieval of it
                const actPromise =
                    objectUnderTest
                        .addConsumerSaleRegDraft(
                            addConsumerSaleRegDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );


                /*
                 act
                 */
                const deleteConsumerSaleRegDraftWithIdPromise =
                    actPromise
                        .then(id =>
                            objectUnderTest
                                .deleteConsumerSaleRegDraftWithId(
                                    id,
                                    factory
                                        .constructValidPartnerRepOAuth2AccessToken()
                                )
                        );

                /*
                 assert
                 */
                deleteConsumerSaleRegDraftWithIdPromise
                    .then(() => {
                        //expect(string).toEqual('Deleted Consumer Draft successfully.');
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });
    });
});