import config from './config';
import jwt from 'jwt-simple';
import dummy from '../dummy';
import ConsumerSaleRegDraftSaleSimpleLineItem from '../../src/consumerSaleRegDraftSaleSimpleLineItem';
import ConsumerSaleRegDraftSaleCompositeLineItem from '../../src/consumerSaleRegDraftSaleCompositeLineItem';
import ConsumerSaleLineItemComponent from '../../src/consumerSaleLineItemComponent';

export default {
    constructValidAppAccessToken,
    constructValidPartnerRepOAuth2AccessToken,
    constructSimpleLineItems,
    constructCompositeLineItems
}

function constructValidAppAccessToken():string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": "app",
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructValidPartnerRepOAuth2AccessToken():string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": 'partnerRep',
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url,
        "given_name": dummy.firstName,
        "family_name": dummy.lastName,
        "sub": dummy.url,
        "account_id": dummy.partnerAccountId,
        "sap_vendor_number": dummy.sap_vendor_number
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructSimpleLineItems():ConsumerSaleRegDraftSaleSimpleLineItem[]{

    return [{
        assetId:dummy.assetId,
        serialNumber:dummy.serialNumber,
        productLineId:dummy.productLineId,
        price:dummy.price,
        productLineName:dummy.productLineName,
        productGroupId:dummy.productGroupId,
        productGroupName:dummy.productGroupName,
        productName:dummy.productName
        }]
    ;
}

function constructSaleLineItemComponents():ConsumerSaleLineItemComponent[]{
    return [{
        assetId:dummy.assetId,
        serialNumber:dummy.serialNumber,
        price:null,
        productLineId:dummy.productLineId,
        productLineName:dummy.productLineName,
        productGroupId:dummy.productGroupId,
        productGroupName:dummy.productGroupName,
        productName:dummy.productName
    }];
}

function constructCompositeLineItems():ConsumerSaleRegDraftSaleCompositeLineItem[]{
    return [{
        components:constructSaleLineItemComponents(),
        price:dummy.price
    }];
}

